import { Component, Input, OnInit } from '@angular/core';
import { ChartData, Color } from 'chart.js';

@Component({
  selector: 'app-dona',
  templateUrl: './dona.component.html',
  styles: [
  ]
})
export class DonaComponent implements OnInit {

  @Input() titulo: string = 'Sin Titulo';

  // Doughnut
  @Input('data') arrData: number[] = [250, 130, 70];
  @Input('labels') arrLabels: string[] = ['label1', 'label2', 'label3'];
  @Input('colors') arrColors: string[] = ['#FFD124', '#0039F5', '#F26A2C'];

  public doughnutChartData: ChartData<'doughnut'> = { datasets: [] };


  constructor() { }

  ngOnInit(): void {
    this.doughnutChartData = {
      labels: this.arrLabels,
      datasets: [
        {
          data: this.arrData,
          backgroundColor: this.arrColors
        },
      ]
    }
  }

}
