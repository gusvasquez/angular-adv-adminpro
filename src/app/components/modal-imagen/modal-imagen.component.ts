import { Component, OnInit } from '@angular/core';
import { FileUploadService } from 'src/app/services/file-upload.service';
import { ModalImagenService } from 'src/app/services/modal-imagen.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-imagen',
  templateUrl: './modal-imagen.component.html',
  styles: [
  ]
})
export class ModalImagenComponent implements OnInit {

  public imagenSubir!: File;
  /* Imagen temporal */
  public imgTemp: any = '';

  constructor(public modalImagenService: ModalImagenService, public fileUploadService: FileUploadService) { }

  ngOnInit(): void {
  }

  cerrarModal(){
    this.imgTemp = null;
    this.modalImagenService.cerrarModal();
  }


  cambiarImagen(event: any) {
    /* Se asigna el valor de la imagen a subir  */
    this.imagenSubir = event.target.files[0];
    /* si no existe un archivo seleccionado */
    if (!event.target.files[0]) {
      /* si cancela la selecciono de la imagen pone como valor null y vuelve y muestra la imagen que tiene el usuario */
      this.imgTemp = null;
      return;
    }
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    /* retorna el nombre de la imagen */
    reader.onloadend = () => {
      /* se asina la base64 de la imagen a la propiedad imgTemp */
      this.imgTemp = reader.result;
    }

  }


  subirImagen() {

    const id = this.modalImagenService.id;
    const tipo = this.modalImagenService.tipo;

    this.fileUploadService.actualizarFoto(this.imagenSubir, tipo!, id!).then(img => {
      Swal.fire('Guardado', 'Imagen Actualizada', 'success');
      /* Emite el valor de la ruta de la imagen */
      this.modalImagenService.nuevaImagen.emit(img);
      this.cerrarModal();
    }, error => {
      Swal.fire('Opsss', error.error.msg, 'error');
    });
  }

}
