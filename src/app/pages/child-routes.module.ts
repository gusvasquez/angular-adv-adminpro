import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { RxjsComponent } from './rxjs/rxjs.component';
import { AdminGuard } from '../guards/admin.guard';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgressComponent } from './progress/progress.component';
import { Grafica1Component } from './grafica1/grafica1.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PromesasComponent } from './promesas/promesas.component';
import { PerfilComponent } from './perfil/perfil.component';
import { UsuariosComponent } from './mantenimientos/usuarios/usuarios.component';
import { HospitalesComponent } from './mantenimientos/hospitales/hospitales.component';
import { MedicosComponent } from './mantenimientos/medicos/medicos.component';
import { MedicoComponent } from './mantenimientos/medicos/medico.component';
import { BusquedasComponent } from './busquedas/busquedas.component';

const children: Routes = [
  { path: '', component: DashboardComponent, data: { titulo: 'DashBoard' } },
  { path: 'progress', component: ProgressComponent, data: { titulo: 'ProgressBar' } },
  { path: 'promesas', component: PromesasComponent, data: { titulo: 'Promesas' } },
  { path: 'grafica1', component: Grafica1Component, data: { titulo: 'Gráficas' } },
  { path: 'perfil', component: PerfilComponent, data: { titulo: 'Perfil' } },
  { path: 'account-settings', component: AccountSettingsComponent, data: { titulo: 'Temas' } },
  { path: 'rxjs', component: RxjsComponent, data: { titulo: 'RxJs' } },
  /* Busquedas */
  { path: 'buscar/:termino', component: BusquedasComponent, data: { titulo: 'Busquedas' } },
  /* Mantenimientos */
  /* Usuarios */
  /* Hospitales */
  { path: 'hospitales', component: HospitalesComponent, data: { titulo: 'Mantenimiento de Hospitales' } },
  /*  Medicos */
  { path: 'medicos', component: MedicosComponent, data: { titulo: 'Mantenimiento de Medicos' } },
  { path: 'medicos/:id', component: MedicoComponent, data: { titulo: 'Mantenimiento de Medicos' } },
  /* RUTAS DE ADMIN */
  { path: 'usuarios', canActivate: [AdminGuard], component: UsuariosComponent, data: { titulo: 'Usurios del Sistema' } },
]


@NgModule({
  imports: [RouterModule.forChild(children)],
  exports: [RouterModule]
})
export class ChildRoutesModule { }
