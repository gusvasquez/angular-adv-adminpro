import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Usuario } from 'src/app/models/usuario.model';
import { FileUploadService } from 'src/app/services/file-upload.service';
import { UsuarioService } from 'src/app/services/usuario.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styles: [
  ]
})
export class PerfilComponent implements OnInit {

  public perfil!: FormGroup;
  public usuario: Usuario;

  public imagenSubir!: File;
  /* Imagen temporal */
  public imgTemp: any = '';


  constructor(private fb: FormBuilder, private usuarioService: UsuarioService, private fileUploadService: FileUploadService) {
    this.usuario = usuarioService.usuario;
  }

  ngOnInit(): void {
    this.perfil = this.fb.group({
      nombre: [this.usuario.nombre, Validators.required],
      email: [this.usuario.email, [Validators.required, Validators.email]],
    })
  }

  actualizarPerfil() {
    this.usuarioService.actualizarPerfil(this.perfil.value).subscribe(
      response => {
        /* se asigna los valores a el modelo de Uusario */
        const { nombre, email } = this.perfil.value;
        this.usuario.nombre = nombre;
        this.usuario.email = email;
        Swal.fire('Guardado', 'Los cambios fueron guardados', 'success');
      }, error => {
        Swal.fire('Opps', error.error.msg, 'error');
      }
    );
  }

  cambiarImagen(event: any) {
    /* Se asigna el valor de la imagen a subir  */
    this.imagenSubir = event.target.files[0];
    /* si no existe un archivo seleccionado */
    if (!event.target.files[0]) {
      /* si cancela la selecciono de la imagen pone como valor null y vuelve y muestra la imagen que tiene el usuario */
      this.imgTemp = null;
      return;
    }
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    /* retorna el nombre de la imagen */
    reader.onloadend = () => {
      /* se asina la base64 de la imagen a la propiedad imgTemp */
      this.imgTemp = reader.result;
    }

  }

  subirImagen() {
    this.fileUploadService.actualizarFoto(this.imagenSubir, 'usuarios', this.usuario.uid!).then(img => {
      this.usuario.image = img;
      Swal.fire('Guardado', 'Imagen Actualizada', 'success');
    }, error => {
      Swal.fire('Opsss', error.error.msg, 'error');
    });
  }

}
