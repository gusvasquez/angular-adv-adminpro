import { Component } from '@angular/core';


@Component({
  selector: 'app-grafica1',
  templateUrl: './grafica1.component.html',
  styles: [
  ]
})
export class Grafica1Component {

  // grafica1
  labels1: string[] = ['Hola', 'In-Store Sales', 'Mail-Order Sales'];
  data1: number[] = [350, 450, 100];
  colors1: string[] = ['#B801F5','#7EE635','#F22CBE'];

  // grafica2
  data2: number[] = [200, 150, 650];
  colors2: string[] = ['#2DFCF3','#7EE635','#F23F2C'];

}
