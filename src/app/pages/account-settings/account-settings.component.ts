import { Component, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styles: [

  ]
})
export class AccountSettingsComponent implements OnInit {



  constructor(private settingsService: SettingsService) { }

  ngOnInit(): void {
    this.settingsService.changeCurrentTheme();
  }

  changeTheme(theme: string) {
    /*  metodo esta en el servicio */
    this.settingsService.changeTheme(theme);
  }

}
