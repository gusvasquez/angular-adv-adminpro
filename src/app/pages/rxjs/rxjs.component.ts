import { Component, OnDestroy } from '@angular/core';
import { filter, interval, map, Observable, retry, Subscription, take } from 'rxjs';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: [
  ]
})
export class RxjsComponent implements OnDestroy{

  public intervalSubs!: Subscription;

  constructor() {


    /*  this.retornaObserbable()
       //PIPE sirve para transformar la informacion que fluye atraves del observable
       .pipe(
         retry(1)
       )
       .subscribe(response => {
         console.log(response);
       }, error => {
         console.warn('Error', error);
       }, () => {
         console.info('Obs terminado');
       }); */
    this.intervalSubs = this.retornaIntervalo().subscribe(response => {
      console.log(response);
    });

  }

  ngOnDestroy() {
    this.intervalSubs.unsubscribe();
  }

  retornaIntervalo() {
    const interval$ = interval(500).pipe(
      /* Trasforma el valor que emite el obserbable */
      map(valor => {
        return valor + 1;
      })
      , filter(valor => {
        /* si el valor dividido entre 0 da cero entonces es true, si no entonces el falso, mostrara solo los valores que cumpplan la condicion */
        return (valor % 2 === 0) ? true : false;
      })
      ,//take(10)
      );
    return interval$;
  }

  retornaObserbable() {
    let i = -1;

    const obs$ = new Observable<number>(observe => {


      const intervalo = setInterval(() => {

        i++;
        observe.next(i);
        if (i === 4) {
          clearInterval(intervalo);
          observe.complete();
        }

        if (i === 2) {
          clearInterval(intervalo);
          observe.error('i llego al 2');
        }
      }, 1000);
    });

    return obs$;

  }

}
