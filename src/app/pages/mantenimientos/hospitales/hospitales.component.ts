import { Component, OnDestroy, OnInit } from '@angular/core';
import { delay, Subscription } from 'rxjs';
import { Hospital } from 'src/app/models/hospital.model';
import { BusquedasService } from 'src/app/services/busquedas.service';
import { HospitaleService } from 'src/app/services/hospitale.service';
import { ModalImagenService } from 'src/app/services/modal-imagen.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-hospitales',
  templateUrl: './hospitales.component.html',
  styles: [
  ]
})
export class HospitalesComponent implements OnInit, OnDestroy {

  public hospitales: Hospital[] = [];
  public hospitalesTemp: Hospital[] = [];
  public cargando: boolean = true;
  public imgSubs!: Subscription;

  constructor(private hospitalService: HospitaleService, private modalImagenService: ModalImagenService, private busquedaService: BusquedasService) { }

  /* Destruir el susbribe cuando se cierre y no tengamos fuga de memoria o se evitar que se cargue varias veces */
  ngOnDestroy(): void {
    this.imgSubs.unsubscribe();
  }

  ngOnInit(): void {
    this.cargarHositales();
    /* Cuando se guarda la imagen emite un valor y va a ejecutar el metodo de cargar usuarios */
    this.imgSubs = this.modalImagenService.nuevaImagen.pipe(delay(100)).subscribe(response => this.cargarHositales());
  }

  cargarHositales() {
    this.cargando = true;

    this.hospitalService.cargarHospitales().subscribe(
      response => {
        this.cargando = false;
        this.hospitales = response;
        this.hospitalesTemp = response;
      }
    );
  }

  guardarCambios(hospital: Hospital) {
    this.hospitalService.actualizarHospital(hospital._id, hospital.nombre).subscribe(
      response => {
        Swal.fire('Actualizado', hospital.nombre, 'success');
      }
    );
  }

  borrarHospital(hospital: Hospital) {
    this.hospitalService.borrarHospital(hospital._id).subscribe(
      response => {
        this.cargarHositales();
        Swal.fire('Borrado', hospital.nombre, 'success');
      }
    );
  }

  async abrirSweetAlert() {
    const { value = '' } = await Swal.fire({
      input: 'text',
      title: 'Crear Hospital',
      inputLabel: 'Ingrese el nombre del nuevo Hospital',
      showCancelButton: true,
      confirmButtonText: 'Guardar',
      inputPlaceholder: 'Nombre del hospital'
    });

    if (value.trim().length > 0) {
      this.hospitalService.crearHospital(value).subscribe(
        (response: any) => {
          this.hospitales.push(response.hospital);
        }
      );
    }
  }

  abrirModal(hospital: Hospital) {
    this.modalImagenService.abrirModal('hospitales', hospital._id!, hospital.image);
  }

  buscar(termino: string) {
    if (termino.length === 0) {
      this.hospitales = this.hospitalesTemp;
      return;
    }

    this.busquedaService.buscar('hospitales', termino).subscribe(
      (response: any[]) => {
        this.hospitales = response;
      }
    )
  }



}
