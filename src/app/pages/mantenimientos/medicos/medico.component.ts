import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { delay } from 'rxjs';
import { Hospital } from 'src/app/models/hospital.model';
import { Medico } from 'src/app/models/medico.model';
import { HospitaleService } from 'src/app/services/hospitale.service';
import { MedicoService } from 'src/app/services/medico.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styles: [
  ]
})
export class MedicoComponent implements OnInit {

  public medicoForm!: FormGroup;
  public hospitales: Hospital[] = [];

  public hospitalSeleccionado: Hospital | undefined;

  public medicoSeleccionado!: Medico;

  constructor(private fb: FormBuilder, private hospitalService: HospitaleService, private medicoService: MedicoService, private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {

    this.activatedRoute.params.subscribe(
      ({ id }) => {
        this.cargarMedico(id);
      }
    );

    this.medicoForm = this.fb.group({
      nombre: ['', [Validators.required]],
      hospital: ['', [Validators.required]]
    });
    this.cargarHospital();
    /* se ejecuta y retorna el valor del hospital seleccionado  */
    this.medicoForm.get('hospital')?.valueChanges.subscribe(
      response => {
        /* Se busca por id */
        this.hospitalSeleccionado = this.hospitales.find(hospital => hospital._id === response);
      }
    );
  }


  cargarMedico(id: string) {
    if (id === 'nuevo') {
      return;
    }
    this.medicoService.cargarMedicosPorID(id).pipe(delay(100)).subscribe(
      response => {
        if (!response) {
          this.router.navigateByUrl(`/dashboard/medicos`);
        }
        this.medicoSeleccionado = response;
        const { nombre, hospital } = response;
        /* Establecemos valores al formulario */
        this.medicoForm.setValue({ nombre: nombre, hospital: hospital?._id });
      }
    );
  }

  cargarHospital() {
    this.hospitalService.cargarHospitales().subscribe(
      (response: Hospital[]) => {
        this.hospitales = response;
      }
    )
  }
  guardarCambios() {
    const { nombre } = this.medicoForm.value;
    /* Actualizar medico */
    if (this.medicoSeleccionado) {
      const data = {
        ...this.medicoForm.value,
        _id: this.medicoSeleccionado._id
      }
      this.medicoService.actualizarMedico(data).subscribe(
        response => {
          console.log(response);
          Swal.fire('Actualizado', `${nombre} actualizado correctamente`, 'success');
        }
      )
    }
    /* Crear Medioc */
    else {
      this.medicoService.crearMedico(this.medicoForm.value).subscribe(
        (response: any) => {
          Swal.fire('Creado', `${nombre}`, 'success');
          this.router.navigateByUrl(`/dashboard/medicos/${response.medico._id}`);
        }
      );
    }

  }

}
