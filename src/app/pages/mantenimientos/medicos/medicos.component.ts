import { Component, OnDestroy, OnInit } from '@angular/core';
import { delay, Subscription } from 'rxjs';
import { Medico } from 'src/app/models/medico.model';
import { BusquedasService } from 'src/app/services/busquedas.service';
import { MedicoService } from 'src/app/services/medico.service';
import { ModalImagenService } from 'src/app/services/modal-imagen.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-medicos',
  templateUrl: './medicos.component.html',
  styles: [
  ]
})
export class MedicosComponent implements OnInit, OnDestroy {

  public medicos: Medico[] = [];
  public medicosTemp: Medico[] = [];
  public cargando: boolean = true;
  public imgSubs!: Subscription;

  constructor(private medicoService: MedicoService, private modalImagenService: ModalImagenService, private busquedaService: BusquedasService) { }

  /* Destruir el susbribe cuando se cierre y no tengamos fuga de memoria o se evitar que se cargue varias veces */
  ngOnDestroy(): void {
    this.imgSubs.unsubscribe();
  }


  ngOnInit(): void {
    this.cargarMedicos();
    this.imgSubs = this.modalImagenService.nuevaImagen.pipe(delay(100)).subscribe(response => this.cargarMedicos());
  }

  buscar(termino: string) {
    if (termino.length === 0) {
      this.medicos = this.medicosTemp;
      return;
    }

    this.busquedaService.buscar('medicos', termino).subscribe(
      (response: any[]) => {
        this.medicos = response;
      }
    )
  }

  abrirModal(medico: Medico) {
    this.modalImagenService.abrirModal('medicos', medico._id!, medico.image);
  }

  cargarMedicos() {
    this.cargando = true;
    this.medicoService.cargarMedicos().subscribe(
      response => {
        this.medicos = response;
        this.medicosTemp = response;
        this.cargando = false;
      }
    );
  }

  borrarMedico(medico: Medico) {
    Swal.fire({
      title: '¿Borrar medico?',
      text: `Esta apunto de borrar ${medico.nombre}`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Si, borrarlo!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.cargarMedicos();
        this.medicoService.borrarMedico(medico._id).subscribe(response => {
          Swal.fire('Medico Borrado', `${medico.nombre} fue eliminado correctamente`, 'success');
        });

      }
    })
  }

}
