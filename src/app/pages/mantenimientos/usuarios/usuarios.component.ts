import { Component, OnDestroy, OnInit } from '@angular/core';
import { delay, Subscription } from 'rxjs';
import { Usuario } from 'src/app/models/usuario.model';
import { BusquedasService } from 'src/app/services/busquedas.service';
import { ModalImagenService } from 'src/app/services/modal-imagen.service';
import { UsuarioService } from 'src/app/services/usuario.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styles: [
  ]
})
export class UsuariosComponent implements OnInit, OnDestroy {

  public totalUsuarios: number = 0;
  public usuarios: Usuario[] = [];
  public usuariosTemp: Usuario[] = [];
  public desde: number = 0;

  public cargando: boolean = true;

  public imgSubs!: Subscription;

  constructor(private usuarioService: UsuarioService, private busquedaService: BusquedasService, private modalImagenService: ModalImagenService) { }

  /* Destruir el susbribe cuando se cierre y no tengamos fuga de memoria o se evitar que se cargue varias veces */
  ngOnDestroy(): void {
    this.imgSubs.unsubscribe();
  }

  ngOnInit(): void {
    this.cargarUsuarios();
    /* Cuando se guarda la imagen emite un valor y va a ejecutar el metodo de cargar usuarios */
    this.imgSubs = this.modalImagenService.nuevaImagen.pipe(delay(100)).subscribe(response => this.cargarUsuarios());
  }

  cargarUsuarios() {
    this.cargando = true;
    this.usuarioService.cargarUsuarios(this.desde).subscribe(
      ({ total, usuarios }) => {
        this.totalUsuarios = total;
        this.usuarios = usuarios;
        this.usuariosTemp = usuarios;
        this.cargando = false;
      });
  }

  cambiarPagina(valor: number) {
    this.desde += valor;

    if (this.desde < 0) {
      this.desde = 0;
    } else if (this.desde >= this.totalUsuarios) {
      this.desde -= valor;
    }
    this.cargarUsuarios();
  }

  buscar(termino: string) {

    if (termino.length === 0) {
      this.usuarios = this.usuariosTemp
      return;
    }

    this.busquedaService.buscar('usuarios', termino).subscribe(
      (response: any[]) => {
        this.usuarios = response;
      }
    )
  }

  eliminarUsuario(usuario: Usuario) {

    if (usuario.uid === this.usuarioService.uid) {
      Swal.fire('Opps', 'No puede borrarse a si mismmo', 'error')
      return;
    }


    Swal.fire({
      title: '¿Borrar usuario?',
      text: `Esta apunto de borrar ${usuario.nombre}`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Si, borrarlo!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.cargarUsuarios();
        this.usuarioService.eliminarUsuario(usuario).subscribe(response => {
          Swal.fire('Usuario Borrado', `${usuario.nombre} fue eliminado correctamente`, 'success');
        });

      }
    })
  }

  cambiarRol(usuario: Usuario) {

    this.usuarioService.guardarUsuario(usuario).subscribe(
      response => {
        Swal.fire('Actualización', `Se actualizo el rol de ${usuario.nombre}`, 'success');
      }
    );
  }

  abrirModal(usuario: Usuario) {
    this.modalImagenService.abrirModal('usuarios', usuario.uid!, usuario.image);
  }

}
