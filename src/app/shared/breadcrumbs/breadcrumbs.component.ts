import { Component, OnDestroy } from '@angular/core';
import { ActivationEnd, Router } from '@angular/router';
import { filter, map, Subscription } from 'rxjs';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styles: [
  ]
})
export class BreadcrumbsComponent implements OnDestroy {

  public titulo: string = '';
  public tituloSub$: Subscription;

  constructor(private router: Router) {
    this.tituloSub$ = this.getArgumentosRuta()
      .subscribe(event => {
        this.titulo = event['titulo'];
        document.title = `AdminPro - ${this.titulo}`;
      });;
  }
  /*  destruir el componente cuando se salga de la ruta o haga el logout */
  ngOnDestroy(): void {
    this.tituloSub$.unsubscribe();
  }

  getArgumentosRuta() {
    return this.router.events
      .pipe(
        filter((event: any) => event instanceof ActivationEnd),
        /*  filtramos y obtenemos solo el ActivationEnd que tiene firstChild en null*/
        filter((event: ActivationEnd) => event.snapshot.firstChild === null),
        /* con el map solo retornamos el arreglo data */
        map((event: ActivationEnd) => event.snapshot.data),
      );
  }


}
