import { AfterViewInit, Component, ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';
import Swal from 'sweetalert2';

declare const google: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    './login.component.css'
  ]
})
export class LoginComponent implements AfterViewInit {

  @ViewChild('googleBtn') googleBtn!: ElementRef;

  public formSubmitted = false;

  public loginForms: FormGroup = this.fb.group({
    email: [localStorage.getItem('email') || '', [Validators.required, Validators.email]],
    password: ['', [Validators.required]],
    remember: [false],
  });

  constructor(private router: Router, private fb: FormBuilder, private usuarioService: UsuarioService, private ngZone: NgZone) { }

  ngAfterViewInit() {
    this.googleInit();
  }

  googleInit() {
    google.accounts.id.initialize({
      client_id: "382412342623-eel9sej33480d4ht9sh111eluujtijup.apps.googleusercontent.com",
      callback: (response: any) => this.handleCredentialResponse(response)
    });
    google.accounts.id.renderButton(
      this.googleBtn.nativeElement,
      { theme: "outline", size: "large" }  // customization attributes
    );
  }

  handleCredentialResponse(response: any) {
    this.usuarioService.loginGoogle(response.credential).subscribe(
      response => {
        this.ngZone.run(()=>{
          this.router.navigateByUrl('/');
        });
      });
  }

  login() {
    this.usuarioService.login(this.loginForms.value).subscribe(
      response => {
        /* Si el marca guardar el email */
        if (this.loginForms.get('remember')?.value) {
          localStorage.setItem('email', this.loginForms.get('email')?.value);
        } else {
          localStorage.removeItem('email');
        }
        // navegar al dashboard
        this.router.navigateByUrl('/');
      }, error => {
        Swal.fire('Error', error.error.msg, 'error');
      }
    );
  }

}
