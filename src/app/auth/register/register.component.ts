import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: [
    './register.component.css'
  ]
})
export class RegisterComponent {

  public formSubmitted = false;

  public registerForm: FormGroup = this.fb.group({
    nombre: ['', [Validators.required, Validators.minLength(3)]],
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required]],
    password2: ['', [Validators.required]],
    terminos: [false, [Validators.required]],
  }, {
    validators: this.passwordIguales('password', 'password2')
  });

  constructor(private fb: FormBuilder, private usuarioService: UsuarioService, private router: Router) { }

  crearUsuario() {
    // si le han hecho submited al formulario
    this.formSubmitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    /* Realizar el posteo */
    this.usuarioService.crearUsuario(this.registerForm.value).subscribe(
      response => {
        this.router.navigateByUrl('/');
      }, error => {
        Swal.fire('Error', error.error.msg, 'error');
      });
  }
  // validacion para los campos
  campoNoValido(campo: string): boolean {
    if (this.registerForm.get(campo)?.invalid && this.formSubmitted) {
      return true;
    } else {
      return false;
    }
  }
  /* terminos y condiciones */
  aceptarTerminos() {
    return !this.registerForm.get('terminos')?.value && this.formSubmitted;
  }
  /* Verificar contraseñas, si no son validas */
  clavesNoValidas() {
    const pass = this.registerForm.get('password')?.value;
    const pass2 = this.registerForm.get('password2')?.value;
    if ((pass !== pass2) && this.formSubmitted) {
      return true;
    } else {
      return false;
    }
  }

  passwordIguales(pass1Name: string, pass2Name: string) {
    return (formGrooup: FormGroup) => {
      const pass1Control = formGrooup.get(pass1Name);
      const pass2Control = formGrooup.get(pass2Name);

      if (pass1Control?.value === pass2Control?.value) {
        pass2Control?.setErrors(null);
      } else {
        pass2Control?.setErrors({ noEsIgual: true });
      }
    }
  }


}
