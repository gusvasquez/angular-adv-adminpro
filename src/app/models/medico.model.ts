import { Hospital } from "./hospital.model";

interface _medicoUser{
  _id: string;
  nombre: string;
  image: string;
}

export class Medico {
  constructor(
    public nombre: string,
    public _id: string,
    public image?: string,
    public usuario?: _medicoUser,
    public hospital?: Hospital
  ) { }
}
