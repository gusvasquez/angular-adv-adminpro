import { environment } from "src/environments/environment";

const baseApiUrl = environment.base_url;

environment

export class Usuario {
  constructor(
    public nombre: string,
    public email: string,
    public password?: string,
    public image?: string,
    public google?: boolean,
    public rol?: 'ADMIN_ROLE' | 'USER_ROLE',
    public uid?: string
  ) { }

  //
  get imagenUrl() {

    if (!this.image) {
      return `${baseApiUrl}/upload/usuarios/no-image`;
    }

    if (this.image?.includes('https')) {
      return this.image;
    }
    if (this.image) {
      return `${baseApiUrl}/upload/usuarios/${this.image}`;
    } else {
      return `${baseApiUrl}/upload/usuarios/no-image`;
    }
  }

}
