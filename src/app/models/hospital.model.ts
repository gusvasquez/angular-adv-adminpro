interface _hospitalUser{
  _id: string;
  nombre: string;
  image: string;
}

export class Hospital {
  constructor(
    public nombre: string,
    public _id: string,
    public image?: string,
    public usuario?: _hospitalUser
  ) { }
}
