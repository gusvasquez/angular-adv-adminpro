import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Hospital } from '../models/hospital.model';
import { Medico } from '../models/medico.model';
import { Usuario } from '../models/usuario.model';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class BusquedasService {

  constructor(private http: HttpClient) { }

  get token(): string {
    return localStorage.getItem('token') || '';
  }

  get headers() {
    return { headers: { 'x-token': this.token } }
  }

  buscar(tipo: 'usuarios' | 'medicos' | 'hospitales', termino: string = '') {
    const url = `${base_url}/todo/coleccion/${tipo}/${termino}`;
    return this.http.get(url, this.headers).pipe(map((response: any) => {
      switch (tipo) {
        case 'usuarios':
          return this.trasformarUsuarios(response.resultados);

        case 'hospitales':
          return this.trasformarHospitles(response.resultados);

        case 'medicos':
          return this.trasformarMedicos(response.resultados);

        default:
          return [];
      }
    }));
  }

  trasformarUsuarios(resultados: any): Usuario[] {
    return resultados.map((user: any) => new Usuario(user.nombre, user.email, '', user.image, user.google, user.rol, user.uid));
  }

  trasformarHospitles(resultados: any): Hospital[] {
    return resultados;
  }

  trasformarMedicos(resultados: any): Medico[] {
    return resultados;
  }

  /* Busqueda global */
  busquedaGlobal(termino: string) {
    const url = `${base_url}/todo/${termino}`;
    return this.http.get(url, this.headers);
  }


}
