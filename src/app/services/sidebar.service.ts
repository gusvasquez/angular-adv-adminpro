import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  public menu: any[] = [];

  /*  menu: any[] = [
     {
       titulo: 'Dashboard', icono: 'mdi mdi-gauge', submenu: [
         { titulo: 'Main', url: '/' },
         { titulo: 'ProgresBar', url: '/dashboard/progress' },
         { titulo: 'Promesas', url: '/dashboard/promesas' },
         { titulo: 'Gráficas', url: '/dashboard/grafica1' },
         { titulo: 'Rxjs', url: '/dashboard/rxjs' }
       ]
     },
     {
       titulo: 'Mantenimientos', icono: 'mdi mdi-folder-lock-open', submenu: [
         { titulo: 'Usuarios', url: 'usuarios' },
         { titulo: 'Hospitales', url: 'hospitales' },
         { titulo: 'Medicos', url: 'medicos' }
       ]
     }
   ] */

  cargarMenu() {
    this.menu = JSON.parse(localStorage.getItem('menu')!);
  }

}
