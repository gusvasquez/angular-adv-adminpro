import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  private element = document.querySelector('#theme');


  constructor() {
    // href="./assets//css/colors/default-dark.css"
    const theme = localStorage.getItem('theme') || './assets//css/colors/default-dark.css';
    this.element?.setAttribute('href', theme);
  }

  changeTheme(theme: string) {
    const url = `./assets//css/colors/${theme}.css`;
    this.element?.setAttribute('href', url);
    localStorage.setItem('theme', url);
    this.changeCurrentTheme();
  }


  changeCurrentTheme() {
    const links = document.querySelectorAll('.selector');
    links.forEach(elem => {
      elem.classList.remove('working');
      const btnTheme = elem.getAttribute('data-theme');
      const btnThemeUrl = `./assets//css/colors/${btnTheme}.css`;
      const curretTheme = this.element?.getAttribute('href');
      if (btnThemeUrl === curretTheme) {
        elem.classList.add('working');
      }
    });
  }

}
