import { EventEmitter, Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class ModalImagenService {

  private _ocultarModal: boolean = true;
  public tipo?: 'usuarios' | 'medicos' | 'hospitales';
  public id?: string;
  public imgActual?: string;
  /* Para emitir un valor cuando cargue la imagen */
  public nuevaImagen: EventEmitter<string> = new EventEmitter<string>;

  get ocultarModal() {
    return this._ocultarModal;
  }

  abrirModal(tipo: 'usuarios' | 'medicos' | 'hospitales', id: string, imgActual: string = 'no-img') {
    this._ocultarModal = false;
    this.tipo = tipo;
    this.id = id;
    //this.imgActual = imgActual;
    if (imgActual?.includes('https')) {
      this.imgActual = imgActual;
    } else {
      this.imgActual = `${base_url}/upload/${tipo}/${imgActual}`;
    }
  }

  cerrarModal() {
    this._ocultarModal = true;
  }

  constructor() { }
}
