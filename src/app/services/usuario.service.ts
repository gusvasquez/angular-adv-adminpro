import { HttpClient } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, delay, map, Observable, of, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CargarUsuario } from '../interfaces/cargar-usuarios.interface';
import { LoginForm } from '../interfaces/login-form.interface';
import { RegisterForm } from '../interfaces/register-form.interface';
import { Usuario } from '../models/usuario.model';

declare const google: any;
const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  public usuario!: Usuario;

  constructor(private http: HttpClient, private router: Router, private ngZone: NgZone) { }

  get token(): string {
    return localStorage.getItem('token') || '';
  }
  // recuperar el uid cuando ya esta logueado
  get uid(): string {
    return this.usuario.uid || '';
  }

  get headers() {
    return { headers: { 'x-token': this.token } }
  }

  guardarLocalStorage(token: string, menu: any) {
    localStorage.setItem('token', token);
    localStorage.setItem('menu', JSON.stringify(menu));
  }

  get rol(): 'ADMIN_ROLE' | 'USER_ROLE' {
    return this.usuario.rol!;
  }

  logout() {
    localStorage.removeItem('token');
    /* BORRAR MENU */
    localStorage.removeItem('menu');
    google.accounts.id.revoke(localStorage.getItem('correo'), (done: any) => {
      this.ngZone.run(() => {
        localStorage.removeItem('correo');
        this.router.navigateByUrl('/login');
      });
    });
  }


  validarToken() {

    return this.http.get(`${base_url}/login/renew`, this.headers).pipe(
      /* tap dispara un efecto secundario */
      map((response: any) => {
        /* Renovamos el TOKEN y ponemos el menu */
        this.guardarLocalStorage(response.token, response.menu);
        const { email, google, image = '', nombre, rol, uid } = response.usuario;
        this.usuario = new Usuario(nombre, email, '', image, google, rol, uid);
        return true;
      }),
      /* Trasformamos la respuesta por que en el guard solo retorna true o false */
      //map(response => true),
      /* Si pasa algun error con el of retornamos con el of el valor de false */
      catchError(error => of(false)));
  }

  crearUsuario(formData: RegisterForm) {
    return this.http.post(`${base_url}/usuarios`, formData).pipe(
      /* tap dispara un efecto secundario */
      tap((response: any) => {
        this.guardarLocalStorage(response.token, response.menu);
      }));
  }

  actualizarPerfil(data: { email: string, nombre: string, rol: string }) {
    /* enviamos todo lo que tiene la data con los ... y le enviamos el rol */
    data = {
      ...data,
      rol: this.usuario.rol!
    }
    return this.http.put(`${base_url}/usuarios/${this.uid}`, data, this.headers);
  }

  login(formData: LoginForm) {
    return this.http.post(`${base_url}/login`, formData).pipe(
      /* tap dispara un efecto secundario */
      tap((response: any) => {
        this.guardarLocalStorage(response.token, response.menu);
      }));
  }
  loginGoogle(token: string) {
    return this.http.post(`${base_url}/login/google`, { token })
      .pipe(
        /* tap dispara un efecto secundario */
        tap((response: any) => {
          localStorage.setItem('correo', response.email);
          /* menu */
          this.guardarLocalStorage(response.token, response.menu);
        }));
  }

  cargarUsuarios(desde: number = 0) {
    const url = `${base_url}/usuarios?desde=${desde}`;
    return this.http.get<CargarUsuario>(url, this.headers)
      .pipe(delay(200), map(response => {
        /* cmabiar el arreglo a un arreglo de tipo usuarios */
        const usuarios = response.usuarios.map(user => new Usuario(user.nombre, user.email, '', user.image, user.google, user.rol, user.uid));
        return {
          total: response.total,
          usuarios
        };
      }));
  }

  eliminarUsuario(usuario: Usuario) {
    const url = `${base_url}/usuarios/${usuario.uid}`;
    return this.http.delete(url, this.headers);
  }

  guardarUsuario(usuario: Usuario) {

    return this.http.put(`${base_url}/usuarios/${usuario.uid}`, usuario, this.headers);
  }


}
